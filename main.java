import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

class Result {

    /*
     * Complete the 'findDay' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts following parameters:
     *  1. INTEGER month
     *  2. INTEGER day
     *  3. INTEGER year
     */

    public static boolean isLeap(int year){
    
    if(year%4 == 0 && year%100 != 0){
        return true;
    } else if (year%400 == 0) {
        return true;
    } else if (year%100 == 0 && year%400 != 0) {
        return false;
    } else {
      return false;
      }
  }


    public static int yearToDays(int year){
        year = year - 2000;
        int days = 0;
        for (int i=1; i<year; i++){
            if(isLeap(i)){
                days += 366;
            } else {
                days += 365;
            }
        }        
      return days%7;
    }    
    
    public static int monthsToDays(int month, int year){
    int[] months = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31} ;
    if (isLeap(year)){
      months[1] = 29;
    }
    int days = 0;
    for (int i=0; i<month-1; i++){
      days += months[i];
    }
    return days%7;
  }

    public static String findDay(int month, int day, int year) {
        int weekday = (yearToDays(year)+ monthsToDays(month, year)+day)%7;
        String wday = "MONDAY";
        switch (weekday){            
            case 1:
                wday = "MONDAY";
                break;
            case 2:
                wday = "TUESDAY";
                break;
            case 3:
                wday = "WEDNESDAY";
                break;
            case 4:
                wday = "THURSDAY";
                break;
            case 5:
                wday = "FRIDAY";
                break;
            case 6:
                wday = "SATURDAY";
                break;
            case 0:
                wday = "SUNDAY";
                break;
  
        }
        return wday;
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] firstMultipleInput = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int month = Integer.parseInt(firstMultipleInput[0]);

        int day = Integer.parseInt(firstMultipleInput[1]);

        int year = Integer.parseInt(firstMultipleInput[2]);

        String res = Result.findDay(month, day, year);

        bufferedWriter.write(res);
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}

